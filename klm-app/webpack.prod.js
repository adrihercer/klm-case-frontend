const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const { BaseHrefWebpackPlugin } = require('base-href-webpack-plugin');

module.exports = merge(common, {
  plugins: [
    new BaseHrefWebpackPlugin({ baseHref: '/travel/pages/' })
  ]
});