# KLM App

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.2.5.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:8081/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. This project uses Webpack to build the artifacts, which be stored in the `dist/` directory.
