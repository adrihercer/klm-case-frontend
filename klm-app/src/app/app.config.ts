export const appConfig = {
    apiFares: 'http://localhost:9000/travel/fares/',
    apiAirports: 'http://localhost:9000/travel/airports',
    apiStats: 'http://localhost:9000/travel/stats'
};