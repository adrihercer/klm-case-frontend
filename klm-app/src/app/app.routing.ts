import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FaresComponent } from './views/fares/fares.component';
import { StatsComponent } from './views/stats/stats.component';
import { AirportsComponent } from './views/airports/airports.component';

const routes: Routes = [
  { path: 'fares', component: FaresComponent },
  { path: 'stats', component: StatsComponent },
  { path: 'airports', component: AirportsComponent },
  { path: '**', redirectTo: 'fares' }
];

@NgModule({
  imports: [
	RouterModule.forRoot(routes)
  ],
  exports: [
	RouterModule
  ]
})

export class AppRoutingModule {}

export const routedComponents = [FaresComponent, StatsComponent, AirportsComponent];
