import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

import { appConfig } from '../app.config';

@Injectable()  
export class AirportsService {
  
  constructor (private httpClient: HttpClient) { }  

  search(keyword) {
    var listOfAirports = this.httpClient.get(appConfig.apiAirports + '/find?keyword=' + keyword)
      .pipe(
        debounceTime(500),
        map(
          (data: any) => {
            return (data.length != 0 ? data as any[] : [{"code":"", "description": ""} as any]);
          }
      ));

      return listOfAirports;
  }

  getAll() {
    var listOfAirports = this.httpClient.get(appConfig.apiAirports + '/all')
      .pipe(
        map(
          (data: any) => {
            return (data.length != 0 ? data as any[] : [{"code":"", "description": "", "name": ""} as any]);
          }
      ));

      return listOfAirports;
  }
}