import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

import { appConfig } from '../app.config';

@Injectable()  
export class StatsService {
  
  constructor (private httpClient: HttpClient) { }  

  getStats() {
    var statsData = this.httpClient.get(appConfig.apiStats)
      .pipe(
        debounceTime(500),
        map(
          (data: any) => {
            return (data.length != 0 ? data as any : {} as any);
          }
      ));

    return statsData;
  }
}