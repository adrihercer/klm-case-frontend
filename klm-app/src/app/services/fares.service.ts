import { Injectable } from '@angular/core';  
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { debounceTime } from 'rxjs/internal/operators/debounceTime';

import { appConfig } from '../app.config';

@Injectable()  
export class FaresService {  

  constructor (private httpClient: HttpClient) { }

  getFare(fromCode, toCode) {
    var fareData = this.httpClient.get(appConfig.apiFares + fromCode + '/' + toCode)
      .pipe(
        debounceTime(500),
        map(
          (data: any) => {
            return (data.length != 0 ? data as any : {"amount":0, "currency": "", "from" : {}, "to": {}} as any);
          }
      ));

      return fareData;
  }
}