import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatsComponent } from './stats.component';
import { StatsRoutingModule } from './stats.routing';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    StatsComponent
  ],
  imports: [
    CommonModule,
    StatsRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [StatsComponent]
})
export class StatsModule { }
