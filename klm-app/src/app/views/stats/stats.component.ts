import { Component } from '@angular/core';

import { StatsService } from '../../services/stats.service'

@Component({
  selector: 'app-root',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css'],
  providers: [StatsService]
})
export class StatsComponent {

  statsData = <any>{};
  
  constructor (private statsService: StatsService) { }

  ngOnInit () {
    this.statsService.getStats().subscribe(
      data => {
        this.statsData = data;
        this.statsData.averageTime = this.statsData.averageResponseData[0] / this.statsData.averageResponseData[1];
        data.httpCodes = [];

        for (var property in this.statsData.responsesByHttpCode) {
          if (this.statsData.responsesByHttpCode.hasOwnProperty(property)) {
            this.statsData.httpCodes.push({
              "code": property,
              "count": this.statsData.responsesByHttpCode[property]
            });
          }
        }
    });
  }
}
