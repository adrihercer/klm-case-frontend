import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaresComponent } from './fares.component';

const routes: Routes = [
  { path: '', component: FaresComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class FaresRoutingModule {
}