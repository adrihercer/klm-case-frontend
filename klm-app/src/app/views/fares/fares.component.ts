import { Component } from '@angular/core';

import { FormControl } from '@angular/forms';
import { AirportsService } from '../../services/airports.service';
import { FaresService } from '../../services/fares.service';

@Component({
  selector: 'app-root',
  templateUrl: './fares.component.html',
  styleUrls: ['./fares.component.css'],
  providers: [AirportsService, FaresService]
})
export class FaresComponent {
  
  fromAirport : FormControl = new FormControl();
  toAirport : FormControl = new FormControl();
  fromAirportsList = <any>[];
  toAirportsList = <any>[];
  fareData = {"from":{}, "to":{}};
  showSpinner = false;
  showFare = false;
  
  constructor (private service: AirportsService, private faresService: FaresService) { }
  
  ngOnInit () {
    this.fromAirport.valueChanges.subscribe(
      term => {
        if (term != '') {
          this.service.search(term).subscribe(
            data => {
              this.fromAirportsList = data as any[];
          })
        }
    });

	  this.toAirport.valueChanges.subscribe(
      term => {
        if (term != '') {
          this.service.search(term).subscribe(
            data => {
              this.toAirportsList = data as any[];
          })
        }
    })
  }
  
  searchFare() {
	  this.showSpinner = true;
	  this.showFare = false;
	  this.faresService.getFare(this.fromAirport.value, this.toAirport.value).subscribe(
      data => {
        this.fareData = data;
			  this.showFare = true;
			  this.showSpinner = false;
    });
  }
}
