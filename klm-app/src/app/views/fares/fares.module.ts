import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FaresComponent } from './fares.component';
import { FaresRoutingModule } from './fares.routing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatInputModule } from '@angular/material';

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    FaresComponent
  ],
  imports: [
    CommonModule,
    FaresRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    MatInputModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [FaresComponent]
})
export class FaresModule { }
