import { Component, OnInit, ViewChild } from '@angular/core';

import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';

import { AirportsService } from '../../services/airports.service'

export interface Airport {
  code: string;
  name: string;
  city: string;
  country: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './airports.component.html',
  styleUrls: ['./airports.component.css'],
  providers: [AirportsService]
})
export class AirportsComponent implements OnInit {

  displayedColumns: string[] = ['code', 'name', 'city', 'country'];
  airportsData: Airport[] = [];
  showSpinner = true;
  dataSource: MatTableDataSource<Airport>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor (private airportsService: AirportsService) { }

  ngOnInit () {
    this.airportsService.getAll().subscribe(
      data => {
        for(var i = 0; i < data.length; i++) {
          this.airportsData.push({
            code: data[i].code,
            name: data[i].name,
            city: data[i].parent.name,
            country: data[i].parent.parent.name
          });
        }
        this.dataSource = new MatTableDataSource(this.airportsData);
        //this.dataSource.sort = this.sort;
        //this.dataSource.paginator = this.paginator;
        setTimeout(() => this.dataSource.sort = this.sort);
        setTimeout(() => this.dataSource.paginator = this.paginator);
        this.showSpinner = false;
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
