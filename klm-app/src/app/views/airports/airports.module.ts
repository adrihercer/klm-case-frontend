import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AirportsComponent } from './airports.component';
import { AirportsRoutingModule } from './airports.routing';

import { MatTableDataSource, MatInputModule } from "@angular/material";

import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AirportsComponent
  ],
  imports: [
    CommonModule,
    AirportsRoutingModule,
    HttpClientModule,
    MatTableDataSource,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AirportsComponent]
})
export class AirportsModule { }
